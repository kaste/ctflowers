from st3m.reactor import Responder
import st3m.run
import leds
from ctx import Context
from st3m.input import InputState
from micropython import const
import binascii
# import uasyncio as asyncio
import bluetooth
import audio

# Sample UUID, use a unique one for your app
SERVICE_UUID = "0000AAAA-0000-1000-8000-00805F9B34FB"
CHARACTERISTIC_UUID = "0000BBBB-0000-1000-8000-00805F9B34FB"  # Another sample UUID
TARGET_SERVICE_UUID = "0000AAAA-0000-1000-8000-00805F9B34FB"
TARGET_CHARACTERISTIC_UUID = "0000BBBB-0000-1000-8000-00805F9B34FB"
MESSAGE_SERVICE_UUID = '6E400001-B5A3-F393-E0A9-E50E24DCCA9E'
MESSAGE_CHARACTERISTIC_UUID = '6E400003-B5A3-F393-E0A9-E50E24DCCA9E'
_ENV_SENSE_UUID = bluetooth.UUID(0x181A)


def get_color(x):
    # create a color based on how high the value is between 0 and 1
    red = int(102 + 153 * x)  # 204-255
    blue = int(255 - (102 * x))  # 153 - 255
    green = 153 - 51 * x  # -102
    # return "rgba(151,187,204,0.2)";
    return [red, green, blue]


class Blinky(Responder):
    def __init__(self) -> None:
        self._current_led_id = 0
        self._delta_time = 0
        self.ble = bluetooth.BLE()
        self.ble.irq(self._irq)
        self.ble.active(True)
        self.data = bytearray(range(16))
        self.ble.gap_advertise(20000, adv_data=self.data, connectable=False)
        self.ble.gap_scan(0)
        self._updated = False

    def _irq(self, event, data):
        _IRQ_SCAN_RESULT = const(5)
        if event == _IRQ_SCAN_RESULT:
            addr_type, addr, adv_type, rssi, adv_data = data
            addr_h = binascii.hexlify(bytes(addr)).decode('utf-8')
            adv_data_h = binascii.hexlify(bytes(adv_data)).decode('utf-8')

            self._updated = True
            self._next_led()

            # print adresses of found signals
            print(addr_h)
            print(adv_data_h)

    def _next_led(self) -> None:
        # sets the current led ID
        self._current_led_id += 1
        if self._current_led_id == 40:
            self._current_led_id = 0
        # render pattern
        self._set_led_pattern()

        return self._current_led_id

    def _set_led_pattern(self) -> None:
        # set current led
        leds.set_rgb(self._current_led_id, *get_color(1))

        # trail length
        trail_length = 15
        # dim previous leds
        for j in range(1, trail_length):
            # circle leds
            jc = self._current_led_id - j
            if jc < 0:
                jc = 40+jc
            # last led
            if j == trail_length-1:
                leds.set_rgb(jc, *get_color(0))
            # set leds
            leds.set_rgb(jc, *get_color((1-(j/trail_length))))

        # update display
        leds.update()

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        # Paint a red square in the middle of the display
        ctx.rgb(255, 0, 0).rectangle(-20, -20, 40, 40).fill()

    def think(self, ins: InputState, delta_ms: int) -> None:

        # testing audio
        audio.input_set_source(audio.INPUT_SOURCE_ONBOARD_MIC)
        for x in dir(audio):
            # print(x)
            pass
        if not self._updated:
            return
        # refresh leds by time delay
        if self._delta_time <= 100:
            self._delta_time += delta_ms
            return
        self._delta_time = 0
        if self._current_led_id == 0:
            self._updated = False
        self._next_led()


st3m.run.run_responder(Blinky())
